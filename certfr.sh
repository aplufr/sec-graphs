#!/bin/bash

### Args: CERT ID
function extract_info ()
{
	FICTMP=$1.txt
	ID=$1
#	CERTFR=http://www.cert.ssi.gouv.fr/site/${ID}/${ID}.html
	CERT=${ID}
#	lynx --dump -width=1024 ${CERTFR} > $FICTMP
#	rCode=$?
#	echo " return code $rCode "
#	[ $rCode -eq 1 ] && return
	title=$(grep -E '^\s*Objet : ' $FICTMP | sed -rn 's/.* dans (.*)/\1/p')
	if [ -z "$title" ] 
	then
		title=$(grep -E '^\s*Objet : ' $FICTMP | sed -rn 's/Objet : (.*)/\1/p')
	fi
	dateprem=$(sed -En '/(Gestion|Historique du).*document/,/\s+(version initiale|première version)/s/^(([0-3]?[0-9]|1er) (janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre|Septembre) 20[0-1][0-9])$/\1/p' $FICTMP)
	if [ -z "${dateprem}" ]
	then
		dateprem=$(sed -rn 's/.*([0-3][0-9] (janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre) 20[0-1][0-9]).*/\1/p' $FICTMP)
	fi
	AVISALERTE=$(grep -q AVI <<< "${CERT}" && echo avis || echo alerte)
	echo "${AVISALERTE^};${CERT};$title;$(dateparse $dateprem);1;$(sed -En 's/^CERT.+-(20[0-1][0-9])-(AVI|ALE.*)-([0-9]+)/\1;\3/p' <<< ${CERT})" 
}

function dateparse ()
{
	YEAR=$3
	local month=$2
	DAY=${1%er}
	local OK=true
	case ${month,,} in
		("janvier") 	MONTH="1" ;;
		("février") 	MONTH="2" ;;
		("mars") 	MONTH="3" ;;
		("avril")	MONTH="4" ;;
		("mai")		MONTH="5" ;;
		("juin")	MONTH="6" ;;
		("juillet")	MONTH="7" ;;
		("août")	MONTH="8" ;;
		("septembre")	MONTH="9" ;;
		("octobre")	MONTH="10" ;;
		("novembre")	MONTH="11" ;;
		("décembre")	MONTH="12" ;;
		*)		OK=false ;;
	esac
	$OK && date -d "$YEAR-$MONTH-$DAY" -I || echo "no date found"
}

while [ $# -gt 0 ]
do
	extract_info ${1%.txt}
	shift
done

